import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import  {img1}  from './avthars/1.jpg';
import  {img2}  from './avthars/2.jpg';
import  {img3}  from './avthars/3.jpg';
import  {img5}  from './avthars/5.jpg';

import { AvatarGroup } from '@material-ui/lab';
import "./GroupAvathars.css"

export default function GroupAvatars() {
  return (
      <div   className ="content"  >
    <AvatarGroup max={4}>
      <Avatar  variant="circle"  alt="Remy Sharp" src={img5} />
      <Avatar alt="Travis Howard" src={img1} />
      <Avatar alt="Cindy Baker" src={img2} />
      <Avatar alt="Agnes Walker" src={img3} />
      <Avatar alt="Trevor Henderson" src="/static/images/avatar/5.jpg" />
    </AvatarGroup>
    </div>
  );
}