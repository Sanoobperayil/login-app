import React, { Component } from 'react'

import GridCard from './GridCard'

export default class Home extends Component {
    render() {
        return (
            <div>
                <GridCard />
            </div>
        )
    }
}
