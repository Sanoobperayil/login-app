import React, { Component } from "react";
import axios from "axios";
import "./Form.css"



export default class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
        username: "",
      password: "",
      displyname: "",
    };
  }
  changeText = (e) => {
    this.setState({ [e.target.name]   : e.target.value })
  };
  register = (e) => {
    e.preventDefault();

    let data = {
      Username: this.state.username,
      Password: this.state.password,
      DisplayName: this.state.displyname,
    };
    
    /*      axios.post(`https://jsonplaceholder.typicode.com/users`, { user })
      .then(res => {
        console.log(res);
        console.log(res.data);
      })   */
  };


  render() {
    return (
      
      <div className="p-3 mb-2 bg-gradient-warning   text-dark">
     
      <div className="container-fluid" >
        
        
        
        
          <div className="col-md-4 offset-md-4">
          <div className="border-top-0">
        <h2>Login Page</h2>
        
        <form>
        
        
        <div className="col-xs-8">
          <div className="form-group">
           
            <label>UserName</label>
            <input
              type="text"
              onChange={this.changeText}
              name="username"
              className="form-control"
            />
          </div>
          <div className="form-group">
            <label>Password</label>
            <input
              type="password"
              onChange={this.changeText}
              name="password"
              className="form-control"
            />
          </div>
          <div className="form-group">
            <label>DisplayName</label>
            <input
              type="text"
              onChange={this.changeText}
              name="displyname"
              className="form-control"
            />
          </div>
          <div className="form-group">
          <button onClick={this.register} type="button" className="btn btn-info">register</button>
          </div>
          </div>
           
          
        
        </form>
      
        
       </div>
      
      </div>
      </div>
      </div>
    
    );
  }
}
