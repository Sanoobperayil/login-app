import React from "react";
import { BrowserRouter as Router,Switch, Route, Link } from "react-router-dom";

import ContactPage from "./ContactPage";
import Form from "./Form";
import Card from "./Card";
import Home from "./Home";

export default function Nav() {
  return (
  
    <div>
      <div>
      <Router>
      <nav className="navbar navbar-expand-sm bg-light  navbar-light">
        <ul className="nav nav-pills nav-fill">
          <li className="nav-item">
          
              <Link to="/home"className="nav-link active"  >Home</Link>
           
          </li>
          <li className="nav-item">
          <Link to="/login"className="nav-link"  >Login</Link>
          </li>
          <li className="nav-item">
          <Link to="/trip"className="nav-link"  >Trip</Link>
          </li>
          <li className="nav-item">
          <Link to="/contact"className="nav-link"  >Contact</Link>
          </li>
        </ul>
        
      </nav>
      <Switch>
          <Route path="/contact">

           <ContactPage />
           
          </Route>
          <Route path="/login">

            <Form />
          </Route>
          <Route path="/trip">
            <Card />
          </Route>

          <Route path ="/home">
          <Home />

          </Route>
         
         
           
          
        </Switch>
      </Router>
      </div>
    </div>
    
  
  );
}
